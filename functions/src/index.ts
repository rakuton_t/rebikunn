import * as functions from 'firebase-functions';

const admin = require('firebase-admin');
admin.initializeApp();

const db = admin.firestore();

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.myFunction = functions.firestore
  .document('user/{userId}')
  .onWrite((change, context) => {
    return db.collection('login_log').add({
        user_id: context.params.userId,
        login_datetime: new Date()
    });
  });
