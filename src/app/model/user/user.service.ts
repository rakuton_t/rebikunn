import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { firestore } from 'firebase';

import { Debug } from 'src/app/util/debug';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  readonly COLLECTION_NAME = 'user';

  public googleUserObject: any[] = null;
  public user: User;
  public managerObservable: Observable<{}>;
  private isManager: boolean;
  public nowProcessing = true;
  constructor(
    public db: AngularFirestore,
    private router: Router) { }

  public setGoogleUserObject(googleUserObject) {
    Debug.log('setGoogleUserObject');
    this.googleUserObject = googleUserObject;
    if (this.googleUserObject) {
      this.user = new User({
        id: this.googleUserObject['uid'],
        name: this.googleUserObject['displayName'],
        email: this.googleUserObject['email']
      });
      console.log('if (this.googleUserObject) {');
      console.log(this.user);
    } else {
      this.user = null;
    }
    Debug.log(this.user);
  }

  public getGoogleUesrObject() {
    return this.googleUserObject;
  }
  public getUser() {
    return this.user;
  }
  public getUserObservable() {
  }

  public LoggedOutThenRedirect(redirectUrl: string = '/'): boolean {
    if (this.IsLoggedIn === false) {
      this.router.navigateByUrl(redirectUrl);
      return true;
    }
    return false;
  }

  public get IsLoggedIn() {
    return (this.googleUserObject != null);
  }
  public setIsManager(isManager: boolean) {
    this.isManager = isManager;
  }
  public get IsManager(): boolean {
    return this.isManager;
  }

  public registUser(user: User = this.user) {
    Debug.log('registUser');
    this.registUserObject(user.getData());
  }
  public registUserObject(user: Object) {
    Debug.log('registUserObject');
    Debug.log(user);
    this.db.collection(this.COLLECTION_NAME).doc(user['id']).set(user);
  }
}
