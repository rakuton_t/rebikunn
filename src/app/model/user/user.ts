import { ModelBase } from '../model-base';

export class User extends ModelBase {
    id: string;
    name: string;
    email: string;
    created: Date;
    updated: Date;
    constructor(object: Object = null) {
        super();

        object = object || {};
        this.id = object['id'] || '';
        this.name = object['name'] || '';
        this.email = object['email'] || '';
        this.created = object['created'] || new Date();
        this.updated = object['updated'] || new Date();
    }
}
