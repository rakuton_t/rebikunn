import { ModelBase } from '../model-base';

export class UserQuizScore extends ModelBase {
    user_id: string;
    quiz_id: string;
    quiz_score: number;
    last_implementation_datetime: Date;
    constructor(object: Object = null) {
        super();
        object = object || {};
        this.user_id = object['user_id'] || '';
        this.quiz_id = object['quiz_id'] || '';
        this.quiz_score = object['quiz_score'] || '';
        this.last_implementation_datetime = object['last_implementation_datetime'] || new Date();
    }
}