// 各モデルで必須の共通機能を定義する基底クラス
export class ModelBase {
    public getData(): object {
        const result = {};
        Object.keys(this).map(key => {
            result[key] = this[key];
            // モデルオブジェクトを連想配列に変換
            if (result[key] instanceof ModelBase) {
                result[key] = result[key].getData();
            }
            // 配列でモデルオブジェクトを持っていた場合も対応する
            if (result[key] instanceof Array) {
                // result[key] = this.getDataArray(result[key]);
                const tmp_array = [];
                result[key].forEach(element => {
                    if (element instanceof ModelBase) {
                        tmp_array.push(element.getData());
                    } else {
                        tmp_array.push(element);
                    }
                });
                result[key] = tmp_array;
            }
        });
        return result;
    }

    public getDataArray(obj: ModelBase[]) {
        return obj.map(o => {
            if (o instanceof ModelBase) {
                return o.getData();
            } else {
                return o;
            }
        });
    }
}
