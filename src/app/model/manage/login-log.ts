import { ModelBase } from '../model-base';
import { firestore } from 'firebase/app';
import Timestamp = firestore.Timestamp;

export class LoginLog extends ModelBase {
    user_id: string;
    login_datetime: Date;
    constructor(object: Object = null) {
        super();
        object = object || {};
        this.user_id = object['user_id'] || '';
        this.login_datetime = object['login_datetime'] || new Date();
        if (this.login_datetime instanceof Timestamp) {
            this.login_datetime = new Date(new Date( this.login_datetime['seconds'] * 1000 ));
        }
    }
}
