import { Injectable } from '@angular/core';
import { AngularFirestore, QuerySnapshot, DocumentData } from 'angularfire2/firestore';
import { Observable, Subject } from 'rxjs';
import { firestore } from 'firebase';

import { Debug } from 'src/app/util/debug';
import { LoginLog } from 'src/app/model/manage/login-log';

@Injectable({
  providedIn: 'root'
})
export class LoginLogService {

  readonly COLLECTION_NAME = 'login_log';
  constructor(public db: AngularFirestore) { }

  // ログインログ一覧を取得
  public getLoginLogSubject(): Subject<LoginLog[]> {
    const loginLogSubject: Subject<LoginLog[]> = new Subject();
    this.db.collection(this.COLLECTION_NAME).get().subscribe(querySnapshot => {
      const loginLogs: LoginLog[] = [];
      Debug.log(querySnapshot);
      querySnapshot.docs.forEach(loginLogData => {
        const loginLog = new LoginLog(loginLogData.data());
        Debug.log('loginLog');
        Debug.log(loginLog);
        loginLogs.push(loginLog);
      });
      loginLogSubject.next(loginLogs);
    });
    return loginLogSubject;
  }

  public getLoginLogObservable() {
    return this.db.collection(this.COLLECTION_NAME).valueChanges();
  }
}
