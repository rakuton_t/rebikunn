import { ModelBase } from '../model-base';

export class Choice extends ModelBase {
    id: string;
    text: string;
    correct: boolean;
    constructor(object: Object = null) {
        super();
        object = object || {};
        this.id = object['id'] || '';
        this.text = object['text'] || '';
        this.correct = object['correct'] || false;
    }
}
