import { ModelBase } from '../model-base';
import { Choice } from './choice';

// クイズの問題クラス
export class Question extends ModelBase {

    id: string;
    question_text: string;
    choices: Choice[];

    constructor(object: Object = null) {
        super();
        object = object || {};
        this.id = object['id'] || '';
        this.question_text = object['question_text'] || '';
        this.choices = object['choices'] || [new Choice()];
        this.choices = this.choices.map(choice => {
            if (choice instanceof Choice) {
                return choice;
            } else {
                return new Choice(choice);
            }
        });

    }
}
