import { ModelBase } from '../model-base';

// クイズのジャンルクラス
export class Genre extends ModelBase {
    id: string;
    name: string;
    quizIds: string[];
    constructor(object: Object = null) {
        super();
        object = object || {};
        this.id = object['id'] || '';
        this.name = object['name'] || '';
        this.quizIds = object['quizIds'] || [];
    }
}
