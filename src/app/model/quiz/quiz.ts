import { ModelBase } from '../model-base';
import { Question } from './question';

// クイズクラス
export class Quiz extends ModelBase {

    id: string;
    name: string;
    questions: Question[];
    genreIds: string[];
    created_user: string;
    created: Date;
    updated: Date;

    constructor(object: Object = null) {
        super();
        object = object || {};
        this.id = object['id'] || '';
        this.name = object['name'] || '';
        this.questions = object['questions'] || [new Question()];
        this.questions = this.questions.map(question => {
            if (question instanceof Question) {
                return question;
            } else {
                return new Question(question);
            }
        });
        this.genreIds = object['genreIds'] || [];
        this.created_user = object['created_user'] || '';
        this.created = object['created'] || new Date();
        this.updated = object['updated'] || new Date();
    }
}
