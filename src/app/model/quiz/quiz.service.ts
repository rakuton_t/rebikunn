
import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from 'angularfire2/firestore';
import { Observable, Subject } from 'rxjs';
import { firestore } from 'firebase';
import { map } from 'rxjs/operators';

import { Debug } from 'src/app/util/debug';
import { Quiz } from 'src/app/model/quiz/quiz';
import { ActionSequence } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  readonly COLLECTION_NAME = 'quiz';
  quizObservable: Observable<Object> = null;
  constructor(public db: AngularFirestore) {
  }

  // 全てのクイズを取得する
  public getQuizzes(): Subject<Quiz[]> {
    const quizzesSubject: Subject<Quiz[]> = new Subject();
    this.db.collection(this.COLLECTION_NAME).get().subscribe(querySnapshot => {
      const quizzes: Quiz[] = [];
      querySnapshot.docs.forEach(quizData => {
        const quiz = new Quiz(quizData.data());
        quiz.id = quizData.id;
        quizzes.push(quiz);
      });
      quizzesSubject.next(quizzes);
    });
    return quizzesSubject;
  }

  // 特定のクイズを取得する
  public getQuiz(id: string): Subject<Quiz> {
    const quizSubject: Subject<Quiz> = new Subject();
    this.db.collection(this.COLLECTION_NAME).doc<Object>(id).get().subscribe(quizData => {
      const quiz = new Quiz(quizData.data());
      quiz.id = id;
      quizSubject.next(quiz);
    });
    return quizSubject;
  }
  // 特定のクイズを取得する
  public getQuizObservable(id: string) {
    return  this.quizObservable = this.db.collection(this.COLLECTION_NAME).doc<Object>(id).valueChanges();
  }
  // 特定の所有者のクイズを取得する
  public getQuizzezByUserId(user_id: string): Observable<Quiz[]> {
    Debug.log('getQuizzezByUserId');
    const quizzesObservable = this.db.collection(this.COLLECTION_NAME, ref => ref.where('created_user', '==', user_id))
      .snapshotChanges().pipe(
        map(actions => actions.map(action => {
          const doc = action.payload.doc;
          const quiz = new Quiz(doc.data());
          quiz.id = doc.id;
          return quiz;
        })));
    return quizzesObservable;
  }

  // クイズを登録する
  public entryQuizPromise(q: Quiz): Promise<string> {
    Debug.log('entryQuizPromise');
    return new Promise((resolve, reject) => {
      this.db.collection(this.COLLECTION_NAME).add(q.getData()).then(ref => {
        resolve(ref.id);
      }).catch(onrefected => {
        reject('');
      });
    });
  }
  // クイズを更新する
  public updateQuizObject(quiz: Object) {
    Debug.log('updateQuizObject');
    Debug.log(quiz);
    return this.db.collection(this.COLLECTION_NAME).doc(quiz['id']).update(quiz);
  }

  // クイズが属するジャンルを追加する
  public unionGenreIds(quiz_id: string, genre_id: string) {
    this.db.collection(this.COLLECTION_NAME).doc(quiz_id).update({
      genreIds: firestore.FieldValue.arrayUnion(genre_id)
    });
  }
  // クイズが属するジャンルを外す
  public removeGenreIds(quiz_id: string, genre_id: string) {
    this.db.collection(this.COLLECTION_NAME).doc(quiz_id).update({
      genreIds: firestore.FieldValue.arrayRemove(genre_id)
    });
  }

  // クイズを削除する
  public deleteQuiz(quiz_id: string) {
    Debug.log('deleteQuiz:', quiz_id);
    this.db.collection(this.COLLECTION_NAME).doc(quiz_id).delete();
  }

}
