import { Injectable } from '@angular/core';
import { AngularFirestore, QuerySnapshot, DocumentData } from 'angularfire2/firestore';
import { Observable, Subject } from 'rxjs';
import { firestore } from 'firebase';

import { Debug } from 'src/app/util/debug';
import { Genre } from 'src/app/model/quiz/genre';

@Injectable({
  providedIn: 'root'
})
export class GenreService {

  readonly COLLECTION_NAME = 'genre';
  constructor(public db: AngularFirestore) { }

  // 特定のジャンルを取得
  public getGenreSubject(id: string): Subject<Genre> {
    const genreSubject: Subject<Genre> = new Subject();
    this.db.collection(this.COLLECTION_NAME).doc<Object>(id).get().subscribe(data => {
      const genre = new Genre(data.data());
      genre.id = id;
      genreSubject.next(genre);
    });
    return genreSubject;
  }

  // ジャンル一覧を取得
  public getGenresSubject(): Subject<Genre[]> {
    const genresSubject: Subject<Genre[]> = new Subject();
    this.db.collection(this.COLLECTION_NAME).get().subscribe(querySnapshot => {
      const genres: Genre[] = [];
      querySnapshot.docs.forEach(genreData => {
        const genre = new Genre(genreData.data());
        genre.id = genreData.id;
        genres.push(genre);
      });
      genresSubject.next(genres);
    });
    return genresSubject;
  }

  // 特定のジャンルを取得
  public getGenreObservable(id: string): Observable<Object> {
    Debug.log('getGenreId:' + id);
    return this.db.collection(this.COLLECTION_NAME).doc<Object>(id).valueChanges();
  }
  public getGenresObservable(): Observable<QuerySnapshot<DocumentData>> {
    Debug.log('getGenres');
    return this.db.collection(this.COLLECTION_NAME).get();
  }


  public setGenre(genre: Genre): void {
    this.setGenreObject(genre.getData());
  }
  public setGenreObject(genre: Object): void {
    Debug.log('setGenreObject');
    Debug.log(genre);
    if (genre['id'] === '') {
      this.db.collection(this.COLLECTION_NAME).add(genre).then(ref => {
        Debug.log("登録完了:", ref);
      }).catch(function(error) {
        Debug.log('登録エラー:', error);
      });
    } else {
      this.db.collection(this.COLLECTION_NAME).doc(genre['id']).update(genre);
    }
  }

  // ジャンルのクイズ一覧にクイズIDを追加
  public unionQuizIds(genre_id: string, quiz_id: string){
    this.db.collection(this.COLLECTION_NAME).doc(genre_id).update({
      quizIds: firestore.FieldValue.arrayUnion(quiz_id)
    });
  }
  // ジャンルのクイズ一覧からクイズIDを削除
  public removeQuizIds(genre_id: string, quiz_id: string){
    this.db.collection(this.COLLECTION_NAME).doc(genre_id).update({
      quizIds: firestore.FieldValue.arrayRemove(quiz_id)
    });
  }

  // ジャンルを削除
  public deleteGenre(id: string) {
    this.db.collection(this.COLLECTION_NAME).doc(id).delete().catch();
  }
}

