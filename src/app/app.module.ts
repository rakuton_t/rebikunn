import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { FlexLayoutModule } from '@angular/flex-layout';

import { AppComponent } from './app.component';
import { QuestionComponent, QuestionDialogComponent } from './quiz/question/question.component';
import { QuizListComponent } from './quiz/quiz-list/quiz-list.component';
import { TopComponent } from './top/top.component';
import { QuizEditComponent } from './quizSetting/quiz-edit/quiz-edit.component';
import { GenreListComponent } from './quizSetting/genre-list/genre-list.component';
import { QuizSettingTopComponent } from './quizSetting/quiz-setting-top/quiz-setting-top.component';
import { QuizSettingListComponent } from './quizSetting/quiz-setting-list/quiz-setting-list.component';
import { QuizTopComponent } from './quiz/quiz-top/quiz-top.component';
import { environment } from '../environments/environment';
import { LoginComponent } from './common/login/login.component';
import { LogoutComponent } from './common/logout/logout.component';
import { ManageTopComponent } from './manage/manage-top/manage-top.component';
import { QuizMypageComponent } from './quiz/quiz-mypage/quiz-mypage.component';
import { QuizEntryComponent, QuizEntryDialogComponent } from './quiz/quiz-entry/quiz-entry.component';
import { TermsOfServiceComponent } from './terms-of-service/terms-of-service.component';

@NgModule({
  declarations: [
    AppComponent,
    QuestionComponent,
    QuestionDialogComponent,
    QuizListComponent,
    TopComponent,
    QuizEditComponent,
    GenreListComponent,
    QuizSettingTopComponent,
    QuizSettingListComponent,
    QuizTopComponent,
    LoginComponent,
    LogoutComponent,
    ManageTopComponent,
    QuizMypageComponent,
    QuizEntryComponent,
    QuizEntryDialogComponent,
    TermsOfServiceComponent
  ],
  entryComponents: [QuestionDialogComponent, QuizEntryDialogComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFirestoreModule,
    AngularFireModule,
    AngularFireModule.initializeApp(environment.firabase, 'Google'),
    AngularFirestoreModule,
    AngularFireAuthModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    FlexLayoutModule,
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
