import { environment } from '../../environments/environment';

export class Debug {
    static log(text: any, ...optionParams: any[]) {
        if (environment.debug) {
            console.log(text, optionParams);
        }
    }
}
