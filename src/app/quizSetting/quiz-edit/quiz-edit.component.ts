// クイズ編集画面

import { Component, OnInit, NgModule } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

import { Debug } from 'src/app/util/debug';
import { Question } from 'src/app/model/quiz/question';
import { Choice } from 'src/app/model/quiz/choice';
import { Quiz } from 'src/app/model/quiz/quiz';
import { QuizService } from 'src/app/model/quiz/quiz.service';
import { Genre } from 'src/app/model/quiz/genre';
import { GenreService } from 'src/app/model/quiz/genre.service';

@Component({
  selector: 'app-quiz-edit',
  templateUrl: './quiz-edit.component.html',
  styleUrls: ['./quiz-edit.component.scss']
})
export class QuizEditComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private meta: Meta,
    private title: Title,
    private genreService: GenreService,
    private quizService: QuizService) {
      this.quizId = this.route.snapshot.paramMap.get('quizId');
    }

  quiz: Quiz = new Quiz({questions: [new Question({id: '1'})]});
  quizId: string;
  genres: Genre[] = [];
  beforeChangeQuiz: Quiz = new Quiz();

  connectingFlg = false;

  ngOnInit() {
    if (this.quizId !== null) {
      this.quizService.getQuizObservable(this.quizId).subscribe(quizData => {
        this.quiz = new Quiz(quizData);
        this.quiz.id = this.quizId;
        this.beforeChangeQuiz = new Quiz(this.quiz.getData());
      });
    } else {
      Debug.log('this.quizId === null');
    }

    this.genreService.getGenresSubject().subscribe(genres => {
      this.genres = genres;
    });
  }

  // 選択肢を追加するボタン
  addChoiceButtonClick(questionIndex: number) {
    Debug.log(questionIndex);
    this.quiz.questions[questionIndex].choices.push(new Choice());
  }

  // 問題を追加するボタン
  addQuestionButtonClick() {
    this.quiz.questions.push(new Question());
  }

  // 選択肢の正解・不正解を変更するボタン
  changeCorrectButtonClick(questionIndex: number, choiceIndex: number) {
    this.quiz.questions[questionIndex].choices[choiceIndex].correct
          = (this.quiz.questions[questionIndex].choices[choiceIndex].correct === false);
  }

  // 選択肢を削除するボタン
  deleteChoiceButtonClick(questionIndex: number, choiceIndex: number) {
    this.quiz.questions[questionIndex].choices.splice(choiceIndex, 1);
  }

  // 問題を削除するボタン
  deleteQuestionButtonClick(questionIndex: number) {
    this.quiz.questions.splice(questionIndex, 1);
  }

  // ジャンルのセレクトボックス変更時時の処理
  genreSelectBoxChange(genreIndex: string) {
    if (genreIndex === '') { return; }
    if (this.quiz.genreIds.find(id => id === this.genres[genreIndex].id)) { return; }

    Debug.log('genreOptionClick');
    this.quiz.genreIds.push(this.genres[genreIndex].id);
    Debug.log(this.quiz);
  }

  // ジャンルIDからジャンルオブジェクトを取得する
  getGenreForGenreId(genreId: string) {
    let genre: Genre;
    this.genres.forEach(data => {
      if (data.id === genreId) {
        genre = data;
      }
    });
    return genre;
  }

  // ジャンル削除
  deleteGenreClick(genreIndex: number) {
    this.quiz.genreIds.splice(genreIndex, 1);
  }

  // クイズ登録ボタン
  entryQuizButtonClick() {
    if (this.connectingFlg) { return; }
    this.connectingFlg = true;

    // 問題IDを設定
    this.quiz.questions.forEach((q, i) => {
      q.id = (i + 1).toString();
    });

    // 登録してクイズIDをセット
    if (this.quiz.id === '') {
      this.quizService.entryQuizPromise(this.quiz).then(quizId => {
        if (quizId === '') {
          Debug.log('通信エラー');
        } else {
          this.quiz.id = quizId;
          this.setGenre();
          this.beforeChangeQuiz = new Quiz(this.quiz.getData());
          this.connectingFlg = false;
          Debug.log(this.quiz);
        }
      });
    } else {
      this.quizService.updateQuizObject(this.quiz.getData()).then(reason => {
        this.connectingFlg = false;
      });
      this.setGenre();
      this.beforeChangeQuiz = new Quiz(this.quiz.getData());
    }
  }
  // 付与したジャンルのドキュメントにクイズIDを追加
  setGenre() {
    // 外されたジャンルを持つための変数
    let deleteGenreIds: string[] = this.beforeChangeQuiz.genreIds;

    // 付与された各ジャンルのクイズID一覧にこのクイズを追加する
    this.quiz.genreIds.forEach(genreId => {
      Debug.log('genreId:', genreId);
      this.genreService.unionQuizIds(genreId, this.quiz.id);
      deleteGenreIds = deleteGenreIds.filter(id => id !== genreId);
    });
    // 外したジャンルのドキュメントからクイズIDを外す
    deleteGenreIds.forEach(genreId => {
      this.genreService.removeQuizIds(genreId, this.quiz.id);
    });
  }

  // クイズ削除ボタンクリック時の処理
  deleteQuizButtonClick() {
    this.quizService.deleteQuiz(this.quiz.id);
    this.beforeChangeQuiz.genreIds.forEach(genreId => {
      this.genreService.removeQuizIds(genreId, this.quiz.id);
    });
    this.quizId = null;
    this.quiz = new Quiz();
    this.beforeChangeQuiz = new Quiz();
  }
}
