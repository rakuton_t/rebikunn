import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizSettingListComponent } from './quiz-setting-list.component';

describe('QuizSettingListComponent', () => {
  let component: QuizSettingListComponent;
  let fixture: ComponentFixture<QuizSettingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizSettingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizSettingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
