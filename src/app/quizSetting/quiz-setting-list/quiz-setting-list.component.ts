import { Component, OnInit, NgModule } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Quiz } from 'src/app/model/quiz/quiz';
import { QuizService } from 'src/app/model/quiz/quiz.service';
import { GenreService } from 'src/app/model/quiz/genre.service';

@Component({
  selector: 'app-quiz-setting-list',
  templateUrl: './quiz-setting-list.component.html',
  styleUrls: ['./quiz-setting-list.component.scss']
})
export class QuizSettingListComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private meta: Meta,
    private  title: Title,
    private quizService: QuizService) { }

    quizzes: Quiz[] = [];

  ngOnInit() {
    this.quizService.getQuizzes().subscribe(quizzes => {
      this.quizzes = quizzes;
    });
  }

}
