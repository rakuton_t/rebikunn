import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizSettingTopComponent } from './quiz-setting-top.component';

describe('QuizSettingTopComponent', () => {
  let component: QuizSettingTopComponent;
  let fixture: ComponentFixture<QuizSettingTopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizSettingTopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizSettingTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
