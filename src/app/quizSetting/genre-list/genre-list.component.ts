import { Component, OnInit, NgModule } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

import { Debug } from 'src/app/util/debug';
import { Genre } from 'src/app/model/quiz/genre';
import { GenreService } from 'src/app/model/quiz/genre.service';
import { QuizService } from 'src/app/model/quiz/quiz.service';

@Component({
  selector: 'app-genre-list',
  templateUrl: './genre-list.component.html',
  styleUrls: ['./genre-list.component.scss']
})
export class GenreListComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private meta: Meta,
    private title: Title,
    private quizService: QuizService,
    private genreService: GenreService) { }

  genres: Genre[] = new Array<Genre>();
  remove_genres: Genre[] = new Array<Genre>();

  ngOnInit() {
    this.genreService.getGenresSubject().subscribe(genres => {
      this.genres = genres;
      Debug.log(this.genres);
    });
  }

  // ジャンルを削除ボタンクリック時の処理
  deleteGenreButtonClick(genreIndex: number) {
    const remove_genre = this.genres.splice(genreIndex, 1)[0];
    if ( remove_genre.id !== '') {
      this.remove_genres.push(remove_genre);
    }
  }

  // ジャンルを追加ボタンクリック時の処理
  addGenreButtonClick() {
    this.genres.push(new Genre());
  }

  // 設定保存ボタンクリック時の処理
  saveGenreSettingButtonClick() {
    this.genres.forEach(genre => {
      if (genre.name !== '') {
        this.genreService.setGenre(genre);
      }
    });
    this.remove_genres.forEach(genre => {
      this.genreService.deleteGenre(genre.id);
    });
  }
}
