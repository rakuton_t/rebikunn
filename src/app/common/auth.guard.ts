import { Injectable } from '@angular/core';
import { CanActivate
  , CanActivateChild
  , CanLoad
  , Route
  , UrlSegment
  , ActivatedRouteSnapshot
  , RouterStateSnapshot
  , UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

import { AngularFireAuth, AngularFireAuthModule } from 'angularfire2/auth';
import { UserService } from 'src/app/model/user/user.service';
import { Router } from '@angular/router';

import { Debug } from 'src/app/util/debug';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(public af: AngularFireAuth,
    public userService: UserService,
    private router: Router) {
      Debug.log('AuthGuard Consutructor');
      this.af.user.subscribe(user => {
        Debug.log('this.af.user.subscribe(user => {');
        Debug.log(user);
        if (user) {
          this.userService.setGoogleUserObject(user);
          this.userService.registUser();
        } else {
          this.userService.setGoogleUserObject(null);
        }
        this.userService.nowProcessing = false;
        this.userService.LoggedOutThenRedirect();
      });
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    Debug.log('canActivate');
    Debug.log(this.userService.user);
    return true;
  }
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    return true;
  }
}
