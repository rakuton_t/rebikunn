import { Component, OnInit } from '@angular/core';
import { AngularFireAuth, AngularFireAuthModule } from 'angularfire2/auth';
import { UserService } from '../../model/user/user.service';
import { Router } from '@angular/router';

import * as firebase from 'firebase';
import { Debug } from 'src/app/util/debug';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(public af: AngularFireAuth,
    public userService: UserService,
    private router: Router) { }

  ngOnInit() {
    this.af.user.subscribe(user => {
      Debug.log(user);
      if (user) {
        this.userService.setGoogleUserObject(user);
        this.userService.registUser();
      } else {
        this.userService.setGoogleUserObject(null);
      }
      this.userService.nowProcessing = false;
    });
  }

  login() {
    this.af.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider())
      .then(result => {
        Debug.log(result);
        this.userService.setGoogleUserObject(result);
      });
  }
}
