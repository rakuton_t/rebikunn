import { Component, OnInit } from '@angular/core';
import { AngularFireAuth, AngularFireAuthModule } from 'angularfire2/auth';
import { UserService } from 'src/app/model/user/user.service';
import { Router } from '@angular/router';

import * as firebase from 'firebase';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(public af: AngularFireAuth,
    public userService: UserService,
    private router: Router) { }

  ngOnInit() {
  }

  logout() {
    this.af.auth.signOut();
    this.userService.setGoogleUserObject(null);
    this.userService.setIsManager(false);
    this.router.navigateByUrl('/');
  }

}
