import { Component, OnInit, NgModule } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Constants } from '../../constants';
import { Question } from 'src/app/model/quiz/question';
import { Choice } from 'src/app/model/quiz/choice';
import { Quiz } from 'src/app/model/quiz/quiz';
import { QuizService } from 'src/app/model/quiz/quiz.service';
import { Genre } from 'src/app/model/quiz/genre';
import { GenreService } from 'src/app/model/quiz/genre.service';

@Component({
  selector: 'app-quiz-list',
  templateUrl: './quiz-list.component.html',
  styleUrls: ['./quiz-list.component.scss']
})
export class QuizListComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private meta: Meta,
    private title: Title,
    private genreService: GenreService,
    private quizService: QuizService) { }
  genreId: string;
  genre: Genre = new Genre();
  quizzes: Quiz[] = [];

  ngOnInit() {
    this.genreId = this.route.snapshot.paramMap.get('jenre');
    if (this.genreId == null) {
      this.genreId = 'all';
    }

    this.genreService.getGenreSubject(this.genreId).subscribe(genre => {
      this.genre = genre;
      this.quizService.getQuizzes().subscribe(quizzes => {
        this.genre.quizIds.forEach(id => {
          const quiz = quizzes.find(q => q.id === id);
          if (quiz !== undefined) {
            this.quizzes.push(quiz);
          }
        });
      });
    });
    this.setDescription('れびが遊びで作ったクイズの一覧です。');
    this.setTitle('れびのクイズ一覧 | れびの遊び場');
  }

  // metaタグdiscription要素を設定
  setDescription(description: string) {
    this.meta.removeTag('name=\'description\'');
    this.meta.addTag({name: 'description', content: description});
  }
  // metaタグtitle要素を設定
  setTitle(title: string) {
    this.title.setTitle(title);
  }
}
