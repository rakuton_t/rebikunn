// クイズのスタート画面と、問題及び点数画面(今後スタート画面と問題画面は切り分ける予定)

import { Component, OnInit, NgModule, Inject } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Question } from 'src/app/model/quiz/question';
import { Choice } from 'src/app/model/quiz/choice';
import { Quiz } from 'src/app/model/quiz/quiz';
import { QuizService } from 'src/app/model/quiz/quiz.service';
import { Debug } from 'src/app/util/debug';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  question: Question;
  correct: boolean;
}

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private meta: Meta,
    private  title: Title,
    private quizService: QuizService,
    public dialog: MatDialog) {
  }

  correctCount = 0; // 正解数
  faildCount = 0; // 誤答数

  quizId: string;
  questionId: string;

  quiz: Quiz = new Quiz();
  question: Question = new Question();

  quizEndFlg = false;
  resultText = '';

  ngOnInit() {
    this.quizId = this.route.snapshot.paramMap.get('quizId');
    this.questionId = this.route.snapshot.paramMap.get('questionId');
    this.quizService.getQuiz(this.quizId).subscribe(quiz => {
      this.quiz = quiz;
      this.question = this.quiz.questions.find(question => question.id === this.questionId);
    });
    this.setDescription(this.quiz.name);
    this.setTitle(this.quiz.name + ' | れびの遊び場');
  }

  // 次の問題へ
  goToTheNextQuestion() {
    this.quizId = this.route.snapshot.paramMap.get('quizId');
    this.questionId = '' + (parseInt(this.questionId, 10) + 1);
    this.question = this.quiz.questions.find(question => question.id === this.questionId);
    // 次の問題が存在しない時、結果画面へ遷移
    if ( this.question == null) {
      this.quizEndFlg = true;
      const point: number = Math.round(this.correctCount / (this.correctCount + this.faildCount) * 100);
      if ( point >= 90 ) {
        this.resultText = point + '点！流石ですね！';
      } else if (point >= 70) {
        this.resultText = point + '点！大体オッケーだけどもう少し！';
      } else {
        this.resultText = point + '点！さては野次馬ですね？';
      }

      this.displayTweetButton();
    }
  }

  // 選択肢をクリックしたときの処理
  choiceClicked(choice_index: string) {
    // const choice = this.question.choices.find(c => c.id === choice_id);
    const choice = this.question.choices[choice_index];
    if (choice.correct) {
      this.correctCount += 1;
    } else {
      this.faildCount += 1;
    }
    this.openDialog(choice.correct);
  }

  // ダイアログオープン
  openDialog(correct: boolean): void {
    Debug.log('openDialog');
    const dialogRef = this.dialog.open(QuestionDialogComponent, {
      width: '250px',
      data: { question: this.question, correct: correct}
    });

    dialogRef.afterClosed().subscribe(result => {
      Debug.log('The dialog was closed');
      this.goToTheNextQuestion();
    });
  }


  displayTweetButton() {
    const url = 'https://rebikunn.firebaseapp.com/quiz/question/' + this.quizId + '/0';

    // Twitterボタン生成
    const element = document.createElement('a'); // aタグを作ります
    element.setAttribute('href', 'https://twitter.com/share?ref_src=twsrc%5Etfw');
    element.setAttribute('class', 'twitter-share-button');
    element.setAttribute('data-size', 'large');
    element.setAttribute('data-text', this.quiz.name + ' ー ' + this.resultText);
    element.setAttribute('data-url', url);
    element.setAttribute('data-hashtags', this.quiz.name + ' #' + this.quiz.name + ' #れびの遊び場' );
    element.setAttribute('data-show-count', 'false');
    element.setAttribute('style', 'margin-left: auto; margin-right: auto;');

    const script = document.createElement('script'); // scriptタグを作ります
    script.async = true;
    script.setAttribute('src', 'https://platform.twitter.com/widgets.js');
    script.setAttribute('charset', 'utf-8');

    // aタグ、scriptタグの順で設置します
    const div = document.getElementById('tweetButtonHere'); // ボタンを置きたい場所の手前の要素を取得
    div.parentNode.insertBefore(element, div.nextSibling); // ボタンを置きたい場所にaタグを追加
    div.parentNode.insertBefore(script, div.nextSibling); // scriptタグを追加してJSを実行し、aタグをボタンに変身させる
  }

  setDescription(description: string) {
    // ブログなどのデータを入れる場合はなんどもdescriptionが追加されてしまうため、
    // descriptionを入れる前に既存のdescriptinoを削除しておく
    this.meta.removeTag('name=\'description\'');
    this.meta.addTag({name: 'description', content: description});
  }

  setTitle(title: string) {
    this.title.setTitle(title);
  }

}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'question-dialog',
  templateUrl: 'question-dialog.component.html',
})
export class QuestionDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<QuestionDialogComponent>,
      @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  public question = this.data.question;
  public correct = this.data.correct;
  onNoClick(): void {
    this.dialogRef.close();
  }

}
