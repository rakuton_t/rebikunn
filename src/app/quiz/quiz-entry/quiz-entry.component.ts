import { Component, OnInit, NgModule, OnChanges, DoCheck, Inject } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

import { AuthGuard } from 'src/app/common/auth.guard';
import { Debug } from 'src/app/util/debug';
import { Question } from 'src/app/model/quiz/question';
import { Choice } from 'src/app/model/quiz/choice';
import { Quiz } from 'src/app/model/quiz/quiz';
import { QuizService } from 'src/app/model/quiz/quiz.service';
import { Genre } from 'src/app/model/quiz/genre';
import { GenreService } from 'src/app/model/quiz/genre.service';
import { AngularFireAuth, AngularFireAuthModule } from 'angularfire2/auth';
import { UserService } from '../../model/user/user.service';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user/user';

import { environment } from 'src/environments/environment';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  quiz_id: string;
}

@Component({
  selector: 'app-quiz-entry',
  templateUrl: './quiz-entry.component.html',
  styleUrls: ['./quiz-entry.component.scss']
})
export class QuizEntryComponent implements OnInit, DoCheck {

  constructor(private route: ActivatedRoute,
    private meta: Meta,
    private title: Title,
    private genreService: GenreService,
    private quizService: QuizService,
    public af: AngularFireAuth,
    public userService: UserService,
    private router: Router,
    public auth: AuthGuard,
    public dialog: MatDialog) {
      this.quizId = this.route.snapshot.paramMap.get('quizId');
    }

  user: User;
  quiz: Quiz = new Quiz({questions: [new Question({id: '1'})]});
  quizId: string;
  genres: Genre[] = [];
  beforeChangeQuiz: Quiz = new Quiz();

  connectingFlg = false;

  ngOnInit() {
    this.user = this.auth.userService.getUser();
    if (this.user && this.quizId !== null) {
      this.quizService.getQuiz(this.quizId).subscribe(quizData => {
        this.quiz = new Quiz(quizData);
        this.quiz.id = this.quizId;
        this.beforeChangeQuiz = new Quiz(this.quiz.getData());
      });
    }

    this.genreService.getGenresSubject().subscribe(genres => {
      this.genres = genres;
    });
  }

  ngDoCheck() {
    Debug.log('ngDoCheck');
    if (this.user) {
      return;
    }
    this.user = this.auth.userService.getUser();
    if (this.user && this.quizId !== null) {
      this.quizService.getQuiz(this.quizId).subscribe(quizData => {
        this.quiz = new Quiz(quizData);
        this.quiz.id = this.quizId;
        this.beforeChangeQuiz = new Quiz(this.quiz.getData());
      });
    }

    Debug.log('ngDoCheckEnd');
  }

  // 選択肢を追加するボタン
  addChoiceButtonClick(questionIndex: number) {
    Debug.log(questionIndex);
    this.quiz.questions[questionIndex].choices.push(new Choice());
  }

  // 問題を追加するボタン
  addQuestionButtonClick() {
    this.quiz.questions.push(new Question());
  }

  // 選択肢の正解・不正解を変更するボタン
  changeCorrectButtonClick(questionIndex: number, choiceIndex: number) {
    this.quiz.questions[questionIndex].choices[choiceIndex].correct
          = (this.quiz.questions[questionIndex].choices[choiceIndex].correct === false);
  }

  // 選択肢を削除するボタン
  deleteChoiceButtonClick(questionIndex: number, choiceIndex: number) {
    this.quiz.questions[questionIndex].choices.splice(choiceIndex, 1);
  }

  // 問題を削除するボタン
  deleteQuestionButtonClick(questionIndex: number) {
    this.quiz.questions.splice(questionIndex, 1);
  }

  // ジャンルのセレクトボックス変更時時の処理
  genreSelectBoxChange(genreIndex: string) {
    if (genreIndex === '') { return; }
    if (this.quiz.genreIds.find(id => id === this.genres[genreIndex].id)) { return; }

    Debug.log('genreOptionClick');
    this.quiz.genreIds.push(this.genres[genreIndex].id);
    Debug.log(this.quiz);
  }

  // ジャンルIDからジャンルオブジェクトを取得する
  getGenreForGenreId(genreId: string) {
    let genre: Genre;
    this.genres.forEach(data => {
      if (data.id === genreId) {
        genre = data;
      }
    });
    return genre;
  }

  // ジャンル削除
  deleteGenreClick(genreIndex: number) {
    this.quiz.genreIds.splice(genreIndex, 1);
  }

  // クイズ登録ボタン
  entryQuizButtonClick() {
    if (this.connectingFlg) { return; }
    this.connectingFlg = true;

    // 作成者を設定
    this.quiz.created_user = this.user.id;

    // 問題IDを設定
    this.quiz.questions.forEach((q, i) => {
      q.id = (i + 1).toString();
    });

    // 登録してクイズIDをセット
    if (this.quiz.id === '') {
      this.quizService.entryQuizPromise(this.quiz).then(quizId => {
        if (quizId === '') {
          Debug.log('通信エラー');
        } else {
          this.quiz.id = quizId;
          this.beforeChangeQuiz = new Quiz(this.quiz.getData());
          this.connectingFlg = false;
          this.openDialog();
          Debug.log(this.quiz);
        }
      });
    } else {
      Debug.log('else');
      Debug.log(this.quiz);
      Debug.log(this.quiz.getData());
      Debug.log('else');
      this.quizService.updateQuizObject(this.quiz.getData()).then(reason => {
        this.connectingFlg = false;
        this.openDialog();
      });
      this.beforeChangeQuiz = new Quiz(this.quiz.getData());
    }
  }
  // 付与したジャンルのドキュメントにクイズIDを追加
  setGenre() {
    // 外されたジャンルを持つための変数
    let deleteGenreIds: string[] = this.beforeChangeQuiz.genreIds;

    // 付与された各ジャンルのクイズID一覧にこのクイズを追加する
    this.quiz.genreIds.forEach(genreId => {
      Debug.log('genreId:', genreId);
      this.genreService.unionQuizIds(genreId, this.quiz.id);
      deleteGenreIds = deleteGenreIds.filter(id => id !== genreId);
    });
    // 外したジャンルのドキュメントからクイズIDを外す
    deleteGenreIds.forEach(genreId => {
      this.genreService.removeQuizIds(genreId, this.quiz.id);
    });
  }

  // クイズ削除ボタンクリック時の処理
  deleteQuizButtonClick() {
    this.quizService.deleteQuiz(this.quiz.id);
    this.beforeChangeQuiz.genreIds.forEach(genreId => {
      this.genreService.removeQuizIds(genreId, this.quiz.id);
    });
    this.quizId = null;
    this.quiz = new Quiz();
    this.beforeChangeQuiz = new Quiz();
  }

    // ダイアログオープン
    openDialog(): void {
      Debug.log('openDialog');
      const dialogRef = this.dialog.open(QuizEntryDialogComponent, {
        width: '600px',
        data: { quiz_id: this.quiz.id }
      });
    }

}


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'quiz-entry-dialog',
  templateUrl: 'quiz-entry-dialog.component.html',
})
export class QuizEntryDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<QuizEntryDialogComponent>,
      @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  public url = 'https://' + environment.firabase.authDomain + '/quiz/question/' + this.data.quiz_id + '/' + '0';

  public quiz_id = this.data.quiz_id;
  onNoClick(): void {
    this.dialogRef.close();
  }

}
