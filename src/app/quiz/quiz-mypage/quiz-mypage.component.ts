import { Component, OnInit, NgModule, OnChanges, DoCheck } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

import { AuthGuard } from 'src/app/common/auth.guard';
import { Constants } from '../../constants';
import { Question } from 'src/app/model/quiz/question';
import { Choice } from 'src/app/model/quiz/choice';
import { Quiz } from 'src/app/model/quiz/quiz';
import { QuizService } from 'src/app/model/quiz/quiz.service';
import { Genre } from 'src/app/model/quiz/genre';
import { GenreService } from 'src/app/model/quiz/genre.service';
import { AngularFireAuth, AngularFireAuthModule } from 'angularfire2/auth';
import { UserService } from '../../model/user/user.service';
import { Router } from '@angular/router';


import { Debug } from 'src/app/util/debug';
import { User } from 'src/app/model/user/user';

@Component({
  selector: 'app-quiz-mypage',
  templateUrl: './quiz-mypage.component.html',
  styleUrls: ['./quiz-mypage.component.scss']
})
export class QuizMypageComponent implements OnInit, OnChanges, DoCheck {

  constructor(private route: ActivatedRoute,
    private meta: Meta,
    private title: Title,
    private genreService: GenreService,
    private quizService: QuizService,
    public af: AngularFireAuth,
    public userService: UserService,
    private router: Router,
    public auth: AuthGuard) { }

    quizzes: Quiz[] = [];
    user: User;

  ngOnInit() {
    Debug.log('ngOninit');
    this.user = this.auth.userService.getUser();
    if (this.user) {
      this.quizService.getQuizzezByUserId(this.user.id).subscribe(quizzesData => {
        quizzesData.forEach(quizData => {
          this.quizzes.push(quizData);
        });
      });
    }
    Debug.log(this.user);
    Debug.log(this.quizzes);
    Debug.log('ngOninitEnd');
  }

  ngOnChanges() {
    Debug.log('ngOnChanges');
    Debug.log('ngOnChangesEnd');
  }

  ngDoCheck() {
    Debug.log('ngDoCheck');
    if (this.user) {
      return;
    }
    this.user = this.auth.userService.getUser();
    Debug.log(this.user);
    if (this.user) {
      this.quizService.getQuizzezByUserId(this.user.id).subscribe(quizzesData => {
        quizzesData.forEach(quizData => {
          Debug.log('this.quizData');
          Debug.log(quizData);
          Debug.log('this.quizData');
          this.quizzes.push(quizData);
        });
      });
      Debug.log(this.quizzes);
    }
    Debug.log('ngDoCheckEnd');
  }

}
