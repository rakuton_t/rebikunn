import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizMypageComponent } from './quiz-mypage.component';

describe('QuizMypageComponent', () => {
  let component: QuizMypageComponent;
  let fixture: ComponentFixture<QuizMypageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizMypageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizMypageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
