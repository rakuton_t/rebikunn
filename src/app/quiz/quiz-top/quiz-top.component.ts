import { Component, OnInit, NgModule } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Constants } from '../../constants';
import { Question } from 'src/app/model/quiz/question';
import { Choice } from 'src/app/model/quiz/choice';
import { Quiz } from 'src/app/model/quiz/quiz';
import { QuizService } from 'src/app/model/quiz/quiz.service';
import { Genre } from 'src/app/model/quiz/genre';
import { GenreService } from 'src/app/model/quiz/genre.service';
import { UserService } from 'src/app/model/user/user.service';

@Component({
  selector: 'app-quiz-top',
  templateUrl: './quiz-top.component.html',
  styleUrls: ['./quiz-top.component.scss']
})
export class QuizTopComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private meta: Meta,
    private  title: Title,
    private genreService: GenreService,
    private quizService: QuizService,
    public userService: UserService) { }

    genres: Genre[] = [];

  ngOnInit() {
    this.genreService.getGenresSubject().subscribe(genres => {
      this.genres = genres;
    });
  }

}
