import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizTopComponent } from './quiz-top.component';

describe('QuizTopComponent', () => {
  let component: QuizTopComponent;
  let fixture: ComponentFixture<QuizTopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizTopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
