import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { UserService } from 'src/app/model/user/user.service';

import { Constants } from 'src/app/constants';

@Component({
  selector: 'app-top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.scss']
})
export class TopComponent implements OnInit {

  constructor(private meta: Meta,
    private  title: Title,
    public userService: UserService) { }

  ngOnInit() {
    this.setDescription('れびが遊びで作ったゲームやツールなどを置いていきます。');
    this.setTitle(Constants.SITE_TITLE + ' | トップページ');
  }

  setDescription(description: string) {
    // ブログなどのデータを入れる場合はなんどもdescriptionが追加されてしまうため、
    // descriptionを入れる前に既存のdescriptinoを削除しておく
    this.meta.removeTag('name=\'description\'');
    this.meta.addTag({name: 'description', content: description});
  }

  setTitle(title: string) {
    this.title.setTitle(title);
  }

}
