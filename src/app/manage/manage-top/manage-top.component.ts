import { Component, OnInit } from '@angular/core';

import { LoginLog } from 'src/app/model/manage/login-log';
import { LoginLogService } from 'src/app/model/manage/login-log.service';

@Component({
  selector: 'app-manage-top',
  templateUrl: './manage-top.component.html',
  styleUrls: ['./manage-top.component.scss']
})
export class ManageTopComponent implements OnInit {

  constructor(private loginLogService: LoginLogService) { }

  loginLogs: LoginLog[];

  ngOnInit() {
    this.loginLogService.getLoginLogObservable().subscribe(datas => {
      this.loginLogs = datas.map(function(data) {
        return new LoginLog(data);
      });
    });
  }

}
