import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageTopComponent } from './manage-top.component';

describe('ManageTopComponent', () => {
  let component: ManageTopComponent;
  let fixture: ComponentFixture<ManageTopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageTopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
