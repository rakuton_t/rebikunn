import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './common/auth.guard';

import { QuestionComponent } from './quiz/question/question.component';
import { QuizListComponent } from './quiz/quiz-list/quiz-list.component';
import { QuizEditComponent } from './quizSetting/quiz-edit/quiz-edit.component';
import { TopComponent } from './top/top.component';
import { GenreListComponent } from './quizSetting/genre-list/genre-list.component';
import { QuizSettingTopComponent } from './quizSetting/quiz-setting-top/quiz-setting-top.component';
import { QuizSettingListComponent } from './quizSetting/quiz-setting-list/quiz-setting-list.component';
import { QuizTopComponent } from './quiz/quiz-top/quiz-top.component';
import { ManageTopComponent } from './manage/manage-top/manage-top.component';
import { QuizMypageComponent } from './quiz/quiz-mypage/quiz-mypage.component';
import { QuizEntryComponent } from './quiz/quiz-entry/quiz-entry.component';
import { TermsOfServiceComponent } from './terms-of-service/terms-of-service.component';

const routes: Routes = [
  { path: '', component: TopComponent },
  { path: 'quiz/question/:quizId/:questionId', component: QuestionComponent },
  { path: 'quiz/quiz-list/:jenre', component: QuizListComponent },
  { path: 'quiz/quiz-list', component: QuizListComponent },
  { path: 'quiz/quiz-top', component: QuizTopComponent },
  { path: 'quiz/quiz-mypage', component: QuizMypageComponent, canActivate: [AuthGuard] },
  { path: 'quiz/quiz-entry', component: QuizEntryComponent, canActivate: [AuthGuard] },
  { path: 'quiz/quiz-entry/:quizId', component: QuizEntryComponent, canActivate: [AuthGuard] },
  { path: 'quizSetting/quiz-edit/:quizId', component: QuizEditComponent },
  { path: 'quizSetting/quiz-edit', component: QuizEditComponent },
  { path: 'quizSetting/genre-list', component: GenreListComponent },
  { path: 'quizSetting', component: QuizSettingTopComponent },
  { path: 'quizSetting/quiz-setting-list', component: QuizSettingListComponent },
  { path: 'manage', component: ManageTopComponent },
  { path: 'termsOfService', component: TermsOfServiceComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
